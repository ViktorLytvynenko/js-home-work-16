let n = Number(prompt("Please type number", ""));
let f0;
let f1;
let f2;

if (n >= "0") {
    function fibonacci(n) {
        f0 = 1;
        f1 = 1;
        for (let i = 3; i <= n; i++) {
            f2 = f0 + f1;
            f0 = f1;
            f1 = f2
        }
        return f1;
    }

    console.log(fibonacci(n))
}
else {
function fibonacciMinus(n) {
    return n <= 1 ? n : fibonacciMinus(n - 1) + fibonacciMinus(n - 2);
}

console.log(fibonacciMinus(n))
}